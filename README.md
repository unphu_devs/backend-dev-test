# Backend Test

Esta es una prueba de evaluación para la posición de `Backend Developer` en el departamento de desarrollo de la UNPHU.

Nota: Al completar la prueba, debes enviar un .zip con la solución del ejercicio a `mhernandez@unphu.edu.do` o compartir el enlace de su repositorio privado permitiendo el acceso a `mhernandez@unphu.edu.do`.

## Prueba

 Se realizara un sistema para Ranking de libros donde usuarios podran publicar la ficha de un libro y otros podran calificar segun su gusto.

 Los requerimientos son los siguientes:
 * Un usuario podrá publicar la ficha de un libro. Los campos son: título, descripción, fecha de publicación y autor.
 * Presentar un listado de los últimos libros publicados.
 * Los usuarios podrán calificar los libros con votaciones (+1, -1).
 
## Requerimientos técnicos

1. Realizar una API.
2. Utilizar base de datos.
3. Proveer un "seed" para la base de datos (libros).
4. Proveer una explicación de cómo configurar su proyecto.

## Reglas

1. Use el lenguaje de programación en el que se sienta más confortable.
2. NO haga uso de ningún framework, un buen desarrollador debe saber seleccionar sus herramientas y ponerlas a funcionar en conjunto.
3. El uso de packages es permitido y alentado.
4. Haga uso de .gitignore, evite al máximo incluir archivos innecesarios.
